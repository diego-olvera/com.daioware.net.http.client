package com.daioware.net.http.client;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import com.daioware.file.FileUtil;

public class Http {
	
	private static Charset defaultCharset=Charset.forName("utf-8");
	
	public static String getContentOfFileOrUrl(String method,String fileOrUrl,
			List<NameValuePair> headers,List<NameValuePair> params,Charset charset) throws IOException {
		File f=FileUtil.getCompatibleFile(fileOrUrl);
		if(f.exists()) {
			return FileUtil.getContents(f);
		}
		else if(fileOrUrl.contains("http:") || fileOrUrl.contains("https:")){
			return Http.getContent(method,fileOrUrl,headers, params, charset);
		}
		else {
			return null;
		}
	}
	public static void post(String url,List<NameValuePair> headers,List<NameValuePair> params,
			Charset charset,HttpResponseHandler handler) {
		HttpClient httpclient = HttpClients.createDefault();
		HttpPost http = new HttpPost(url);
		if(headers!=null) {
			for(NameValuePair header:headers) {
				http.addHeader(header.getName(),header.getValue());
			}
		}
		if(params!=null) {
			http.setEntity(new UrlEncodedFormEntity(params,charset));
		}
		if(charset==null) {
			charset=defaultCharset;
		}
		HttpResponse response;
		byte buffer[]=new byte[1024];
		try {
			response = httpclient.execute(http);
			HttpEntity entity = response.getEntity();
			int bytesRead;
			if (entity != null) {
			    InputStream instream;
				try {
					instream = entity.getContent();
					handler.setHttpEntity(entity);
					try {
						handler.beforeStartRequest();
						while((bytesRead=instream.read(buffer))>=1) {
							handler.acceptPartOfResponse(buffer,bytesRead);
						}
				    } finally {
				        instream.close();
						handler.finishReceivingResponse();
				    }
				} catch (UnsupportedOperationException | IOException e) {
					throw e;
				}
			    
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}	
	}
	private static String getUrlParams(List<NameValuePair> params,Charset charset) {		
		StringBuilder info=new StringBuilder();
		String separator="";
		if(params.size()>=1) {
			info.append("?");
		}
		for(NameValuePair obj:params) {
			info.append(separator); 
			try {
				info.append(obj.getName()).append("=")
					.append(URLEncoder.encode(obj.getValue(),charset.name()));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			separator="&";
		}
		return info.toString();
	}
	public static void get(String url,List<NameValuePair> headers,List<NameValuePair> params,
			Charset charset,HttpResponseHandler handler) {
		HttpClient httpclient = HttpClients.createDefault();
		HttpGet http = new HttpGet(url+(params!=null?getUrlParams(params, charset):""));
		if(headers!=null) {
			for(NameValuePair header:headers) {
				http.addHeader(header.getName(),header.getValue());
			}
		}
		HttpResponse response;
		byte buffer[]=new byte[1024];
		try {
			response = httpclient.execute(http);
			HttpEntity entity = response.getEntity();
			int bytesRead;
			if (entity != null) {
			    InputStream instream;
				try {
					instream = entity.getContent();
					handler.setHttpEntity(entity);
					try {
						handler.beforeStartRequest();
						while((bytesRead=instream.read(buffer))>=1) {
							handler.acceptPartOfResponse(buffer,bytesRead);
						}
				    } finally {
				        instream.close();
						handler.finishReceivingResponse();
				    }
				} catch (UnsupportedOperationException | IOException e) {
					e.printStackTrace();
				}
			    
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}		
	}
	public static String getContentWithGet(String url,List<NameValuePair> headers,List<NameValuePair> params,
			Charset charset) {
		StringBuilder info=new StringBuilder();
		HttpResponseHandler handler=new HttpResponseHandler() {			
			@Override
			public void finishReceivingResponse() {				
			}
			@Override
			public void beforeStartRequest() {				
			}
			@Override
			public void acceptPartOfResponse(byte[] bytes,int bytesRead) {
				info.append(new String(bytes,0,bytesRead,charset));
			} 
		};
		get(url, headers, params, charset, handler);
		return info.toString();
	}
	public static String getContentWithPost(String url,List<NameValuePair> headers,List<NameValuePair> params,
			Charset charset) {
		StringBuilder info=new StringBuilder();
		HttpResponseHandler handler=new HttpResponseHandler() {			
			@Override
			public void finishReceivingResponse() {				
			}
			@Override
			public void beforeStartRequest() {				
			}
			@Override
			public void acceptPartOfResponse(byte[] bytes,int bytesRead) {
				info.append(new String(bytes,0,bytesRead,charset));
			} 
		};
		post(url, headers, params, charset, handler);
		return info.toString();
	}
	public static String getContent(String requestMethod,String url,List<NameValuePair> headers,List<NameValuePair> params,
			Charset charset) {
		switch(requestMethod.toUpperCase()) {
			case "POST":return getContentWithPost(url, headers, params, charset);
			case "GET":default:return getContentWithGet(url, headers, params, charset);
		}
	}
	public static void request(String requestMethod,String url,List<NameValuePair> headers,List<NameValuePair> params,
			Charset charset,HttpResponseHandler handler) {
		switch(requestMethod.toUpperCase()) {
			case "POST":post(url, headers, params, charset, handler);break;
			case "GET":default:get(url, headers, params, charset, handler);
		}
	}
	public static File downloadFile(String requestMethod,String url,List<NameValuePair> headers,List<NameValuePair> params,
			Charset charset,File folder) {
		FileDownloaderHandler handler;
		handler=folder.isDirectory()?new FileDownloaderHandler(url,folder):new FileDownloaderHandler(folder);
		request(requestMethod, url, headers, params, charset, handler);
		return handler.getFile();
	}
	public static void main(String[] args) {
		String url="http://localhost:8080/ServletTest/test";
		List<NameValuePair> headers=new ArrayList<>();
		List<NameValuePair> params=new ArrayList<>();
		Charset charset=Charset.forName("UTF-8");
		headers.add(new BasicNameValuePair("Accept","application/xml"));
		params.add(new BasicNameValuePair("Hello", "Hola como estás"));
		params.add(new BasicNameValuePair("Hello2", "Hola como estás 2"));
		//headers=null;
		String response=Http.getContent("post",url, headers, params, charset);
		System.out.println("Response:\n<"+response+">");

		url="http://api-catalogo.cre.gob.mx/api/utiles/municipios?EntidadFederativaId=01";
		//File folder=new File(System.getProperty("user.dir")+File.separator+"mun.xml");
		File folder=new File(System.getProperty("user.dir"));
		Http.downloadFile("get", url, headers, params, charset,folder);
		
	}
}
