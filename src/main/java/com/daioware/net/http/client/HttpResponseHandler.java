package com.daioware.net.http.client;

import org.apache.http.HttpEntity;

public interface HttpResponseHandler{
	default void setHttpEntity(HttpEntity entity) {
	}
	void beforeStartRequest();
	void acceptPartOfResponse(byte[] bytes,int bytesRead);
	void finishReceivingResponse();
}
