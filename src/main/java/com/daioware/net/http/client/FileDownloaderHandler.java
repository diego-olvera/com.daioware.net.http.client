package com.daioware.net.http.client;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;

import com.daioware.file.MyRandomAccessFile;

public class FileDownloaderHandler implements HttpResponseHandler{
	private static final Map<String, String> fileExtensionMap;

	static {
	    fileExtensionMap = new HashMap<String, String>();
	    fileExtensionMap.put("application/json","json");
	    fileExtensionMap.put("application/xml", "xml");
	    fileExtensionMap.put("application/pdf", "pdf");
	    fileExtensionMap.put("text/plain", "txt");
	    fileExtensionMap.put("text/html", "html");

	}
	private File file;
	private MyRandomAccessFile raoFile;
	private String fileName1;
	private boolean specificFile;
	
	public FileDownloaderHandler(String url,File folder) {
		this(folder);
		int lastIndex=url.indexOf("?");
		if(lastIndex<0) {
			lastIndex=url.length();
		}
		int firstIndex=url.lastIndexOf("/");
		if(firstIndex<0) {
			firstIndex=-1;
		}
		fileName1=url.substring(firstIndex+1, lastIndex);
		specificFile=false;
	}
	public FileDownloaderHandler(File file) {
		this.file = file;
		specificFile=true;
	}
	
	public void setHttpEntity(HttpEntity entity) {
		if(!specificFile) {
			String value=entity.getContentType().getValue();
			int index;
			String ext;
			index=value.indexOf(";");
			if(index<0) {
				index=0;
			}
			else {
				value=value.substring(0,index);
			}
			ext=fileExtensionMap.get(value);
			if(ext==null) {
				ext="txt";
			}
			file=new File(file.getAbsolutePath()+File.separator+fileName1+"."+ext);
		}
		try {
			raoFile=new MyRandomAccessFile(file, "rw");
			raoFile.setLength(0);
		} catch (IOException e) {
			e.printStackTrace();
			raoFile=null;
		}
		finally {
			try {
				if(raoFile==null)raoFile.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	@Override
	public void beforeStartRequest() {
		
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	@Override
	public void acceptPartOfResponse(byte[] bytes, int bytesRead) {
		try {
			if(raoFile!=null)raoFile.write(bytes, 0, bytesRead);
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}

	@Override
	public void finishReceivingResponse() {
		try {
			if(raoFile!=null)raoFile.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
}
